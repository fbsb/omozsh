_k() {
    kubectl $@
}

ktx() {
    local flag=$1

    if [[ $flag =~ "^--all$|^-a$" ]]; then
        _k config get-contexts -o name
    else
        _k config current-context
    fi
}

sktx() {
    local ctx=$1

    if [[ -z "$ctx" ]]; then
        _k config unset current-context
    else
        _k config use $ctx
    fi
}

kns() {
    local flag=$1
    local ctx

    ctx=$(_k config current-context)
    [[ -z $ctx ]] && return 1

    if [[ $flag =~ "^--all$|^-a$" ]]; then
        _k get ns -o json | jq -r '.items[].metadata.name'
    else
        # _k config view -o jsonpath="{.contexts[?(@.name==\"$ctx\")].context.namespace}"
        _k config view -o json | jq -r ".contexts[] | select(.name==\"$ctx\") | .context.namespace" | sed 's/null/default/'
    fi
}

skns() {
    local ns=${1:-"default"}
    local ctx

    ctx=$(_k config current-context)
    [[ -z $ctx ]] && return 1

    n_ns=$(kns -a | grep $ns)
    if [[ -z "$n_ns" ]]; then
        echo "namespace ${ns} does not exist" 1>&2
        return 1
    fi

    _k config set "contexts.${ctx}.namespace" "$ns"
}
